import 'package:flutter/material.dart';
import 'package:jayaj_clo/dashboard.dart';

class Complete extends StatefulWidget {
  const Complete({ Key? key }) : super(key: key);

  @override
  _CompleteState createState() => _CompleteState();
}

class _CompleteState extends State<Complete> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: const Color(0xFFEAE5DF),
        body: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/images/complete.png',
                    fit: BoxFit.cover,
                    width: 220,
                    height: 220,
                  ),
                  const SizedBox(height: 20,),
                  const Text('Thanks for Buying!', style: TextStyle(fontSize: 35, color: Colors.black, fontWeight: FontWeight.bold),),
                  const Text('Go to profile to see your order', style: TextStyle(fontSize: 15, color: Colors.black,),),
                ],
              )
            ),
            SizedBox(
              width: double.infinity,
              height: 80,
              child: ElevatedButton(
                style: ButtonStyle(
                  elevation: MaterialStateProperty.all<double>(0),
                ),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const Dashboard()));
                },
                child: const Text('Continue Shopping', style: TextStyle(fontSize: 25)),
              ),
            ),
          ],
        ),
      ),      
      
    );
  }
}