import 'package:flutter/material.dart';
import 'package:jayaj_clo/details.dart';

class Shirt extends StatefulWidget {
  const Shirt({ Key? key }) : super(key: key);

  @override
  _ShirtState createState() => _ShirtState();
}

class _ShirtState extends State<Shirt> {
  final List<String> shirtname = [
      'Longsleeve with Shirt',
      'Solid Color Round...',
      'Stripe Oversize T-shirt',
      'Black Cat Shirt',
      'Black Hoodie',
      'Green Polo Shirt',
      'Black Shirt',
      'Quiksilver',
      'Chill Vibes',
      'Boston',
    ];
    final List<String> price = [
      'Php 140.00',
      'Php 120.00',
      'Php 140.00',
      'Php 140.00',
      'Php 160.00',
      'Php 120.00',
      'Php 200.00',
      'Php 250.00',
      'Php 200.00',
      'Php 200.00',
    ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: const Color(0xFFEAE5DF),
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: Colors.black,),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: const Text('Shirts', textAlign: TextAlign.center, style: TextStyle(color: Colors.black),),
          centerTitle: true,
          backgroundColor: const Color(0xFFEAE5DF),
          elevation: 0,
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center, 
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 30, left: 30),
              child: TextField(
                onSubmitted: (value) {
                  
                },
                decoration: InputDecoration(
                  hintText: 'Search',
                  prefixIcon: const Icon(Icons.search,),
                  filled: true,
                  fillColor: Colors.grey[200],
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 10),
            
            Expanded(
              child: GridView.builder(
                itemCount: 10,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  mainAxisSpacing: 5,
                  crossAxisSpacing: 5,
                  childAspectRatio: 1,
                ),
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => const Details()));
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: Image.asset(
                            'assets/images/s$index.png',
                            height: 150,
                            fit: BoxFit.cover,
                          ),
                        ),
                        Text(
                          shirtname[index],
                          style: const TextStyle(fontSize: 16),
                        ),
                        Text(
                          price[index],
                          style: const TextStyle(fontSize: 16, color: Colors.red),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
            const SizedBox(height: 16),
          ],
        ),
      ),
      
    );
  }
}