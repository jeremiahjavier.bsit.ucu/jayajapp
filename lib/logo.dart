import 'package:flutter/material.dart';
import 'package:flutter_animated_splash/flutter_animated_splash.dart';
import 'package:jayaj_clo/login.dart';

class Logo extends StatefulWidget {
  const Logo({ Key? key }) : super(key: key);

  @override
  _LogoState createState() => _LogoState();
}

class _LogoState extends State<Logo> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: AnimatedSplash(
        type: Transition.fade, 
        curve: Curves.fastLinearToSlowEaseIn,
        backgroundColor:const Color(0xffFFEAD8),
        navigator: const Login(),
        durationInSeconds: 4,
        child: Image.asset('assets/images/jayajlogo.png', width: 280, height: 250,)
      )
      
    );
  }
}