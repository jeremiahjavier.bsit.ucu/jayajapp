import 'package:flutter/material.dart';

class Shoes extends StatefulWidget {
  const Shoes({ Key? key }) : super(key: key);

  @override
  _ShoesState createState() => _ShoesState();
}

class _ShoesState extends State<Shoes> {
  final List<String> shoesname = [
      'World Balance',
      'Nike',
    ];
    final List<String> price = [
      'Php 1,650.00',
      'Php 1,990.00',
    ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: const Color(0xFFEAE5DF),
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: Colors.black,),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: const Text('Shoes', textAlign: TextAlign.center, style: TextStyle(color: Colors.black),),
          centerTitle: true,
          backgroundColor: const Color(0xFFEAE5DF),
          elevation: 0,
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center, 
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 30, left: 30),
              child: TextField(
                onSubmitted: (value) {
                  
                },
                decoration: InputDecoration(
                  hintText: 'Search',
                  prefixIcon: const Icon(Icons.search,),
                  filled: true,
                  fillColor: Colors.grey[200],
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 10),
            
            Expanded(
              child: GridView.builder(
                itemCount: 2,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  mainAxisSpacing: 5,
                  crossAxisSpacing: 5,
                  childAspectRatio: 1,
                ),
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () {
                      
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: Image.asset(
                            'assets/images/shoes$index.jpg',
                            height: 150,
                            fit: BoxFit.cover,
                          ),
                        ),
                        Text(
                          shoesname[index],
                          style: const TextStyle(fontSize: 16),
                        ),
                        Text(
                          price[index],
                          style: const TextStyle(fontSize: 16, color: Colors.red),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
            const SizedBox(height: 16),
          ],
        ),
      ),
    );
  }
}