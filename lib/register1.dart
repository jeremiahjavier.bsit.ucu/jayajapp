import 'package:flutter/material.dart';
import 'package:jayaj_clo/register2.dart';

class Register1 extends StatefulWidget {
  const Register1({ Key? key }) : super(key: key);

  @override
  _Register1State createState() => _Register1State();
}

class _Register1State extends State<Register1> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
            leading: IconButton(
              icon: const Icon(Icons.arrow_back, color: Colors.black,),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            backgroundColor: Colors.white,
            elevation: 0,
          ),
        body: Center(
          child: SingleChildScrollView(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Text('Hello!', style: TextStyle(fontSize: 35, color: Colors.black, fontWeight: FontWeight.bold),),
                const SizedBox(height: 20,),
                const Text('Setting up your account', style: TextStyle(fontSize: 25, color: Colors.black),),
                const SizedBox(height: 50,),
                Container(
                  alignment: Alignment.centerLeft,
                  child: const Text('Step 1: Login Information', style: TextStyle(fontSize: 20, color: Colors.black),),
                ),
                const SizedBox(height: 20,),
                TextField(
                  decoration: InputDecoration(
                    hintText: 'Username',
                    prefixIcon: const Icon(Icons.person, color: Colors.grey,),
                    filled: true,
                    fillColor: const Color(0xffFFF9F9),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                      borderSide: BorderSide.none,                          
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                      borderSide: const BorderSide(
                        color: Color(0xFFFFEAD8),
                        width: 2.0,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 10,),
                TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    hintText: 'Password',
                    prefixIcon: const Icon(Icons.lock, color: Colors.grey,),
                    suffixIcon: const Icon(Icons.remove_red_eye, color: Colors.grey,),
                    filled: true,
                    fillColor: const Color(0xffFFF9F9),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                      borderSide: BorderSide.none,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                      borderSide: const BorderSide(
                        color: Color(0xFFFFEAD8),
                        width: 2.0,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 10,),
                TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    hintText: 'Confirm Password',
                    prefixIcon: const Icon(Icons.check, color: Colors.grey,),
                    suffixIcon: const Icon(Icons.remove_red_eye, color: Colors.grey,),
                    filled: true,
                    fillColor: const Color(0xffFFF9F9),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                      borderSide: BorderSide.none,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                      borderSide: const BorderSide(
                        color: Color(0xFFFFEAD8),
                        width: 2.0,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 10,),
                SizedBox(
                  child: ElevatedButton(
                  onPressed: () { Navigator.push(context, MaterialPageRoute(builder: (context) => const Register2())); },
                  style: ElevatedButton.styleFrom(
                      elevation: 0,
                      backgroundColor: Colors.black,
                      textStyle: const TextStyle(fontSize: 20)),
                  child: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'DONE',
                      style: TextStyle(fontSize: 18.0),
                    ),
                  ),
                ),
                ),
                const SizedBox(height: 100,),
              ],
            ),
          ),
        )
      ),
      
    );
  }
}