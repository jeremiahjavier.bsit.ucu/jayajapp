import 'package:flutter/material.dart';
import 'package:jayaj_clo/cart.dart';
import 'package:jayaj_clo/cart1.dart';
import 'package:jayaj_clo/checkout.dart';
import 'package:jayaj_clo/complete.dart';
import 'package:jayaj_clo/dashboard.dart';
import 'package:jayaj_clo/dashboardcontent.dart';
import 'package:jayaj_clo/details.dart';
import 'package:jayaj_clo/login.dart';
import 'package:jayaj_clo/logo.dart';
import 'package:jayaj_clo/myorder.dart';
import 'package:jayaj_clo/pants.dart';
import 'package:jayaj_clo/profile.dart';
import 'package:jayaj_clo/register1.dart';
import 'package:jayaj_clo/register2.dart';
import 'package:jayaj_clo/shirt.dart';
import 'package:jayaj_clo/shoes.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/logo',
        routes: {
          '/logo': (context) => const Logo(),
          '/login': (context) => const Login(),
          '/register1': (context) => const Register1(),
          '/register2': (context) => const Register2(),
          '/dashboard': (context) => const Dashboard(),
          '/dashboardcontent': (context) => const Dashboardcontent(),
          '/shirt': (context) => const Shirt(),
          '/pants': (context) => const Pants(),
          '/shoes': (context) => const Shoes(),
          '/details': (context) => const Details(),
          '/cart': (context) => const Cart(),
          '/cart1': (context) => const Cart1(),
          '/checkout': (context) => const Checkout(),
          '/complete': (context) => const Complete(),
          '/profile': (context) => const Profile(),
          '/myorder': (context) => const Myorder(),
        }
      
    );
  }
}
