import 'package:flutter/material.dart';
import 'package:jayaj_clo/dashboard.dart';
import 'package:jayaj_clo/register1.dart';

class Login extends StatefulWidget {
  const Login({ Key? key }) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.white,
        body: Center(
          child: SingleChildScrollView(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(height: 100,),
                const Text('Welcome!', style: TextStyle(fontSize: 35, color: Colors.black, fontWeight: FontWeight.bold),),
                const SizedBox(height: 20,),
                const Text('Sign in to continue', style: TextStyle(fontSize: 30, color: Colors.black),),
                const SizedBox(height: 50,),
                TextField(
                  decoration: InputDecoration(
                    hintText: 'Email',
                    prefixIcon: const Icon(Icons.mail, color: Colors.grey,),
                    filled: true,
                    fillColor: const Color(0xffFFF9F9),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                      borderSide: BorderSide.none,                          
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                      borderSide: const BorderSide(
                        color: Color(0xFFFFEAD8),
                        width: 2.0,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 20,),
                TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    hintText: 'Password',
                    prefixIcon: const Icon(Icons.lock, color: Colors.grey,),
                    suffixIcon: const Icon(Icons.remove_red_eye, color: Colors.grey,),
                    filled: true,
                    fillColor: const Color(0xffFFF9F9),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                      borderSide: BorderSide.none,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                      borderSide: const BorderSide(
                        color: Color(0xFFFFEAD8),
                        width: 2.0,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 10,),
                Row(
                  children: [
                    const SizedBox(width: 10,),
                    const Icon(Icons.check_box, color: Colors.black,),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: const Text('Remember me'),
                    ),
                  ],
                ),
                const SizedBox(height: 10,),
                SizedBox(
                  child: ElevatedButton(
                  onPressed: () { Navigator.push(context, MaterialPageRoute(builder: (context) => const Dashboard())); },
                  style: ElevatedButton.styleFrom(
                      elevation: 0,
                      backgroundColor: Colors.black,
                      textStyle: const TextStyle(fontSize: 20)),
                  child: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'LOG IN',
                      style: TextStyle(fontSize: 18.0),
                    ),
                  ),
                ),
                ),
                const SizedBox(height: 50,),
                Row(
                  children: const [
                  Expanded(
                    child: Divider( thickness: 2.0, )),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8.0),
                      child: Text(
                      'or',
                      style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Expanded(child: Divider( thickness: 2.0 )),
                  ],
                ),
                const SizedBox(height: 10,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const Dashboard()));
                      },
                      child: ClipOval(
                        child: Image.asset(
                          'assets/icon/googleicon.png', // Replace with your image path
                          height: 50,
                          width: 50,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const Dashboard()));
                      },
                      child: ClipOval(
                        child: Image.asset(
                          'assets/icon/fbicon.png', // Replace with your image path
                          height: 50,
                          width: 50,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const Dashboard()));
                      },
                      child: ClipOval(
                        child: Image.asset(
                          'assets/icon/appleicon.png', // Replace with your image path
                          height: 50,
                          width: 50,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 100,),
                SizedBox(
                  child: ElevatedButton(
                  onPressed: () { Navigator.push(context, MaterialPageRoute(builder: (context) => const Register1())); },
                  style: ElevatedButton.styleFrom(
                      backgroundColor: const Color(0xFFD9D9D9),
                      textStyle: const TextStyle(fontSize: 20)),
                  child: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'Create an account',
                      style: TextStyle(fontSize: 18.0, color: Colors.black),
                    ),
                  ),
                ),
                ),
                const SizedBox(height: 100,),
              ],
            ),
          ),
        )
      ),
    );
  }
}