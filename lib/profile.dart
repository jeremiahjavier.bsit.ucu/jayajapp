import 'package:flutter/material.dart';
import 'package:jayaj_clo/myorder.dart';

class Profile extends StatefulWidget {
  const Profile({ Key? key }) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(scaffoldBackgroundColor: const Color(0xffF8F8F8)),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: const Color(0xFFEAE5DF),
        appBar: AppBar(
          title: const Text('Profile', textAlign: TextAlign.center, style: TextStyle(color: Colors.black),),
          centerTitle: true,
          backgroundColor: const Color(0xFFEAE5DF),
          elevation: 0,
        ),
        body: SingleChildScrollView(
          child: Center(
            child: Column(
              children: [
                const SizedBox(height: 10,),
                ClipOval(
                  child: Image.asset(
                    'assets/images/pic.jpg', // Replace with your image path
                    height: 150,
                    width: 150,
                    fit: BoxFit.cover,
                  ),
                ),
                const SizedBox(height: 50,),
                Container(
                  width: double.infinity,
                  height: 25,
                  color: Colors.black,
                  child: const Center(
                    child: Text('Jeremiah Javier', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white),),
                  ),
                ),
                const SizedBox(height: 30,),
                Container(
                  width: double.infinity,
                  height: 100,
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(width: 160,
                      child: Column(
                        children: const [
                          SizedBox(height: 10,),
                          Text('Username', style: TextStyle(fontSize: 15, color: Colors.blue),),
                          SizedBox(height: 15,),
                          Text('Jayat', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                        ],
                      ),
                      ),
                      const SizedBox(width: 20,),
                      Container(width: 5, height: double.infinity, color: const Color(0xffF8F8F8),),
                      const SizedBox(width: 20,),
                      SizedBox(width: 160, 
                      child: Column(
                        children: const [
                          SizedBox(height: 10,),
                          Text('Email Address', style: TextStyle(fontSize: 15, color: Colors.blue),),
                          SizedBox(height: 15,),
                          Text('jay@gmail.com', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                        ],
                      ),),
                      
                    ],
                  )
                ),
                const SizedBox(height: 5,),
                Container(
                  width: double.infinity,
                  height: 100,
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(width: 160,
                      child: Column(
                        children: const [
                          SizedBox(height: 10,),
                          Text('Phone Number', style: TextStyle(fontSize: 15, color: Colors.blue),),
                          SizedBox(height: 15,),
                          Text('09234567891', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                        ],
                      ),
                      ),
                      const SizedBox(width: 20,),
                      Container(width: 5, height: double.infinity, color: const Color(0xffF8F8F8),),
                      const SizedBox(width: 20,),
                      SizedBox(width: 160, 
                      child: Column(
                        children: const [
                          SizedBox(height: 10,),
                          Text('Birthday', style: TextStyle(fontSize: 15, color: Colors.blue),),
                          SizedBox(height: 15,),
                          Text('January 30, 2001', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                        ],
                      ),),
                    ],
                  )
                ),
                const SizedBox(height: 20,),
                Container(
                  height: 50,
                  color: Colors.white,
                  child: Row(
                    children: [
                      const SizedBox(width: 10,),
                      const Icon(Icons.location_on, size: 30, color:Colors.blue,),
                      const SizedBox(width: 10,),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          SizedBox(height: 5,),
                          Text('Address', style: TextStyle(fontSize: 12, color: Colors.blue),),
                          Text('Don Ramon Magsaysay High way', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                        ],
                      ),
                      const SizedBox(width: 20,),
                      const Icon(Icons.arrow_right, size: 30, color: Colors.blue,),
                    ],
                  ),
                ),

                const SizedBox(height: 20,),
                SizedBox(
                  width: double.infinity,
                  height: 45,
                  child: ElevatedButton(
                    onPressed: () { Navigator.push(context, MaterialPageRoute(builder: (context) => const Myorder())); },
                    style: ElevatedButton.styleFrom(
                      elevation: 0,
                      backgroundColor: Colors.black,
                      textStyle: const TextStyle(fontSize: 20),
                    ),
                    child: const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        'My Orders',
                        style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),

                const SizedBox(height: 20,),
              ],
            ),
          ),
        ),
      ),
      
    );
  }
}