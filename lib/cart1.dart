import 'package:flutter/material.dart';
import 'package:jayaj_clo/checkout.dart';

class Cart1 extends StatefulWidget {
  const Cart1({ Key? key }) : super(key: key);

  @override
  _Cart1State createState() => _Cart1State();
}

class _Cart1State extends State<Cart1> {
  int _count = 0;
  final TextEditingController _countController = TextEditingController();
  int _count2 = 0;
  final TextEditingController _countController2 = TextEditingController();

  @override
  void initState() {
    super.initState();
    _countController.text = _count.toString();
    _countController2.text = _count2.toString();
  }

  @override
  void dispose() {
    _countController.dispose();
    _countController2.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: const Color(0xFFEAE5DF),
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: Colors.black,),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: const Text('My Cart', textAlign: TextAlign.center, style: TextStyle(color: Colors.black),),
          centerTitle: true,
          backgroundColor: const Color(0xFFEAE5DF),
          elevation: 0,
        ),
        body: Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 150,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10), 
                              color: Colors.white,
                            ),
                            child: Row(
                              children: [
                                ClipRRect(
                                  borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                  ),
                                  child: Image.asset(
                                    'assets/images/w1.jpg',
                                    fit: BoxFit.cover,
                                    width: 150,
                                    height: 150,
                                  ),
                                ),
                                const SizedBox(width: 5,),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    const SizedBox(height: 5,),
                                    const Text('Longsleeve with Shirt', style: TextStyle(fontSize: 18)),
                                    const Text('Size: XL', style: TextStyle(fontSize: 15)),
                                    const Text('Color: White', style: TextStyle(fontSize: 15)),
                                    const SizedBox(height: 10,),
                                    const Text('Php 280.00', style: TextStyle(fontSize: 18, color: Color(0xFF49E630), fontWeight: FontWeight.bold),),
                                    const SizedBox(height: 10,),
                                    Row(
                                      children: [
                                        Container(
                                          height: 25,
                                          width: 115,
                                          decoration: BoxDecoration(
                                            border: Border.all(color: const Color(0xFF49E630)),
                                            borderRadius: BorderRadius.circular(4),
                                          ),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              IconButton(
                                                icon: const Icon(Icons.remove, size: 10),
                                                onPressed: () {
                                                  setState(() {
                                                    if (_count > 0) {
                                                      _count--;
                                                      _countController.text = _count.toString(); // Update the TextField value
                                                    }
                                                  });
                                                },
                                              ),
                                              SizedBox(
                                                width: 15,
                                                child: TextField(
                                                  controller: _countController,
                                                  keyboardType: TextInputType.number,
                                                  onChanged: (newValue) {
                                                    setState(() {
                                                      _count = int.tryParse(newValue) ?? _count;
                                                    });
                                                  },
                                                  decoration: const InputDecoration(
                                                    border: InputBorder.none,
                                                  ),
                                                  textAlign: TextAlign.center,                                                                                          ),
                                              ),
                                              IconButton(
                                                icon: const Icon(Icons.add, size: 10),
                                                onPressed: () {
                                                  setState(() {
                                                    _count++;
                                                    _countController.text = _count.toString(); // Update the TextField value
                                                  });
                                                },
                                              ),
                                            ],
                                          ),
                                        ),

                                        const SizedBox(width: 50,),

                                        const Icon(Icons.check_box, color: Color(0xFF49E630),)
                                      ],
                                    )
                                  ],
                                )
                              ],
                            )
                          ),

                          const SizedBox(height: 10,), //next List_____________________________

                          Container(
                            height: 150,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10), 
                              color: Colors.white,
                            ),
                            child: Row(
                              children: [
                                ClipRRect(
                                  borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                  ),
                                  child: Image.asset(
                                    'assets/images/w2.jpg',
                                    fit: BoxFit.cover,
                                    width: 150,
                                    height: 150,
                                  ),
                                ),
                                const SizedBox(width: 5,),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    const SizedBox(height: 5,),
                                    const Text('Longsleeve with Shirt', style: TextStyle(fontSize: 18)),
                                    const Text('Size: XL', style: TextStyle(fontSize: 15)),
                                    const Text('Color: Blue', style: TextStyle(fontSize: 15)),
                                    const SizedBox(height: 10,),
                                    const Text('Php 140.00', style: TextStyle(fontSize: 18, color: Color(0xFF49E630), fontWeight: FontWeight.bold),),
                                    const SizedBox(height: 10,),
                                    Row(
                                      children: [
                                        Container(
                                          height: 25,
                                          width: 115,
                                          decoration: BoxDecoration(
                                            border: Border.all(color: const Color(0xFF49E630)),
                                            borderRadius: BorderRadius.circular(4),
                                          ),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              IconButton(
                                                icon: const Icon(Icons.remove, size: 10),
                                                onPressed: () {
                                                  setState(() {
                                                    if (_count2 > 0) {
                                                      _count2--;
                                                      _countController2.text = _count2.toString(); // Update the TextField value
                                                    }
                                                  });
                                                },
                                              ),
                                              SizedBox(
                                                width: 15,
                                                child: TextField(
                                                  controller: _countController2,
                                                  keyboardType: TextInputType.number,
                                                  onChanged: (newValue) {
                                                    setState(() {
                                                      _count2 = int.tryParse(newValue) ?? _count2;
                                                    });
                                                  },
                                                  decoration: const InputDecoration(
                                                    border: InputBorder.none,
                                                  ),
                                                  textAlign: TextAlign.center,                                                                                          ),
                                              ),
                                              IconButton(
                                                icon: const Icon(Icons.add, size: 10),
                                                onPressed: () {
                                                  setState(() {
                                                    _count2++;
                                                    _countController2.text = _count2.toString();
                                                  });
                                                },
                                              ),
                                            ],
                                          ),
                                        ),

                                        const SizedBox(width: 50,),

                                        const Icon(Icons.check_box_outline_blank, color: Color(0xFF49E630),)
                                      ],
                                    )
                                  ],
                                )
                              ],
                            )
                          ),
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      height: 80,
                      color: Colors.black,
                        child: Row(
                          children: [
                            const Expanded(
                              child: Padding(
                                padding: EdgeInsets.only(left: 16.0,),
                                child: Text(
                                  'Total: Php 280.00',
                                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Color(0xFF49E630)),
                                ),
                              ),
                            ),
                            ElevatedButton.icon(
                              onPressed: () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) => const Checkout()));
                              },
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all<Color>(const Color(0xFF49E630)), // Change button background color
                                padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                                  const EdgeInsets.symmetric(vertical: 14.0, horizontal: 40.0), // Change button padding
                                ),
                              ),
                              icon: const Icon(Icons.shopping_cart_checkout, color: Colors.black,), // Add icon
                              label: const Text(
                                'BUY',
                                style: TextStyle(fontSize: 16.0, color: Colors.black), // Change button text size
                              ),
                            ),
                            const SizedBox(width: 5,)
                          ],
                        ),
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}