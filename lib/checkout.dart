import 'package:flutter/material.dart';
import 'package:jayaj_clo/complete.dart';

class Checkout extends StatefulWidget {
  const Checkout({ Key? key }) : super(key: key);

  @override
  _CheckoutState createState() => _CheckoutState();
}

class _CheckoutState extends State<Checkout> {
  String _selectedValue1 = 'Cash on Delivery';
  String _selectedValue2 = '123 Villa, Poz Pang';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: const Color(0xFFEAE5DF),
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: Colors.black,),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: const Text('Shirts', textAlign: TextAlign.center, style: TextStyle(color: Colors.black),),
          centerTitle: true,
          backgroundColor: const Color(0xFFEAE5DF),
          elevation: 0,
        ),
          body: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Padding(padding: EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Container(
                          height: 100,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10), 
                            color: Colors.white,
                          ),
                          child: Row(
                            children: [
                              ClipRRect(
                                borderRadius: const BorderRadius.only(
                                  topLeft: Radius.circular(10),
                                  bottomLeft: Radius.circular(10),
                                ),
                                child: Image.asset(
                                  'assets/images/w1.jpg',
                                  fit: BoxFit.cover,
                                  width: 100,
                                  height: 100,
                                ),
                              ),
                              const SizedBox(width: 5,),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: const [
                                  Text('Longsleeve with Shirt', style: TextStyle(fontSize: 15),),
                                  Text('Size: XL', style: TextStyle(fontSize: 13),),
                                  Text('Color: White', style: TextStyle(fontSize: 13),),
                                  Text('2 items', style: TextStyle(fontSize: 13),)
                                ],
                              ),
                              const SizedBox(width: 15,),
                              const Text('Php 280.00', style: TextStyle(fontSize: 18, color: Color(0xFF49E630), fontWeight: FontWeight.bold),)
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                ),
              ),
              Container(
                height: 200,
                color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const SizedBox(width: 5,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        
                        const SizedBox(height: 5,),
                        const Text('Payment Method', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                        const SizedBox(height: 20,),
                        Container(
                          width: 250,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black,
                              width: 1.0,
                            ),
                            borderRadius: BorderRadius.circular(4),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                              value: _selectedValue1,
                              items: <String>['Cash on Delivery', 'Gcash', 'Bank'].map((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Center(child: Text(value)),
                                );
                              }).toList(),
                              onChanged: (String? newValue) {
                                setState(() {
                                  _selectedValue1 = newValue ?? _selectedValue1;
                                });
                              },
                            ),
                          ),
                        ),
                        const SizedBox(height: 20,),
                        Container(
                          width: 250,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black,
                              width: 1.0,
                            ),
                            borderRadius: BorderRadius.circular(4),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                              value: _selectedValue2,
                              items: <String>['123 Villa, Poz Pang', '150 Alipangpang, Poz Pang', '213 street, Cavity'].map((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Center(child: Text(value)),
                                );
                              }).toList(),
                              onChanged: (String? newValue) {
                                setState(() {
                                  _selectedValue2 = newValue ?? _selectedValue2;
                                });
                              },
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                )
              ),
              Container(
                height: 60,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: const Color(0xFF49E630),
                    width: 2.0,
                  ),
                  color: Colors.white
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    const Text('2 items', style: TextStyle(fontSize: 15, color: Colors.black),),
                    const SizedBox(width: 10,),
                    const Text('Php 280.00', style: TextStyle(fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold),),
                    const SizedBox(width: 50,),
                    SizedBox(
                      width: 150,
                      height: 100,
                      // height: double.infinity,
                      child: ElevatedButton(
                      onPressed: () { Navigator.push(context, MaterialPageRoute(builder: (context) => const Complete())); },
                      style: ElevatedButton.styleFrom(
                          elevation: 0,
                          backgroundColor: const Color(0xFF49E630),
                          textStyle: const TextStyle(fontSize: 20)),
                      child: const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          'Place order',
                          style: TextStyle(fontSize: 18.0, color: Colors.black),
                        ),
                      ),
                    ),
                  ),
                  ],
                )
              ),
            ],
          ),
      ),
    );
  }
}