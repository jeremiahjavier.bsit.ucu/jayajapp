import 'package:flutter/material.dart';

class Myorder extends StatefulWidget {
  const Myorder({ Key? key }) : super(key: key);

  @override
  _MyorderState createState() => _MyorderState();
}

class _MyorderState extends State<Myorder> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: const Color(0xFFEAE5DF),
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: Colors.black,),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: const Text('My Orders', textAlign: TextAlign.center, style: TextStyle(color: Colors.black),),
          centerTitle: true,
          backgroundColor: const Color(0xFFEAE5DF),
          elevation: 0,
        ),
        body: SingleChildScrollView(
          child: Padding(padding: EdgeInsets.all(10),
            child: Column(
              children: [
                Container(
                  height: 150,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10), 
                    color: Colors.white,
                  ),
                  child: Row(
                    children: [
                      ClipRRect(
                        borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(10),
                          bottomLeft: Radius.circular(10),
                        ),
                        child: Image.asset(
                          'assets/images/w1.jpg',
                          fit: BoxFit.cover,
                          width: 150,
                          height: 150,
                        ),
                      ),
                      const SizedBox(width: 5,),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const [
                          SizedBox(height: 5,),
                          Text('Longsleeve with Shirt', style: TextStyle(fontSize: 18),),
                          Text('Size: XL  Color: White', style: TextStyle(fontSize: 15),),
                          SizedBox(height: 10,),
                          Text('Php 280.00', style: TextStyle(fontSize: 18, color: Colors.blue, fontWeight: FontWeight.bold),),
                          Text('2 items', style: TextStyle(fontSize: 14, color: Colors.blue),),
                          SizedBox(height: 9,),
                          Text('The seller is now preparing your', style: TextStyle(fontSize: 14, color: Colors.grey),),
                          Text('order', style: TextStyle(fontSize: 14, color: Colors.grey),),
                        ],
                      )
                    ],
                  )
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}