import 'package:flutter/material.dart';
import 'package:jayaj_clo/cart1.dart';

class Details extends StatefulWidget {
  const Details({Key? key}) : super(key: key);

  @override
  _DetailsState createState() => _DetailsState();
}

const List<Widget> size = <Widget>[
  Text('S'),
  Text('M'),
  Text('L'),
  Text('XL'),
];
const List<Widget> color = <Widget>[
  Text('White'),
  Text('Red'),
  Text('Green'),
];

class _DetailsState extends State<Details> {
  final List<bool> _selectedSize = <bool>[true, false, false, false];
  final List<bool> _selectedColor = <bool>[true, false, false];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: const Color(0xFFEAE5DF),
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: Colors.black,),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: const Text('Details', textAlign: TextAlign.center, style: TextStyle(color: Colors.black),),
          centerTitle: true,
          backgroundColor: const Color(0xFFEAE5DF),
          elevation: 0,
          actions: [
            IconButton(
              icon: const Icon(Icons.favorite_border_outlined, color: Colors.black,),
              onPressed: () {

              },
            ),
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.only(left: 0, right: 0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 300,
                  width: 300,
                  child: SlidingImages(),
                ),

                Container(
                  alignment: Alignment.centerLeft,
                  color: const Color(0xFFD9D9D9),
                  width: double.infinity,
                  height: 400,
                  child: Padding(padding: EdgeInsets.only(left: 10, right: 5),
                    child: Column(
                      children: [
                        const SizedBox(height: 20,),
                        Row(
                          children: const [
                            Text('PHP 140.00', style: TextStyle(fontSize: 30, color: Colors.black),),
                            SizedBox(width: 10,),
                            Text('PHP 349.00', style: TextStyle(fontSize: 15, color: Colors.red, decoration: TextDecoration.lineThrough),),
                          ],
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          child: const Text('Longsleeve with Shirt', style: TextStyle(fontSize: 20, color: Colors.black),),
                        ),

                        const SizedBox(height: 20,),

                        Container(
                            alignment: Alignment.centerLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text('Size', style: TextStyle(fontSize: 20, color: Colors.black),),
                                ToggleButtons(
                                  direction: Axis.horizontal,
                                  onPressed: (int index) {
                                    setState(() {
                                      for (int i = 0; i < _selectedSize.length; i++) {
                                        _selectedSize[i] = i == index;
                                      }
                                    });
                                  },
                                  borderRadius: const BorderRadius.all(Radius.circular(8)),
                                  selectedBorderColor: Colors.green[700],
                                  selectedColor: Colors.white,
                                  fillColor: Colors.green[200],
                                  color: Colors.green[400],
                                  constraints: const BoxConstraints(
                                    minHeight: 40.0,
                                    minWidth: 80.0,
                                  ),
                                  isSelected: _selectedSize,
                                  children: size,
                                ),
                              ],
                            )
                        ),

                        const SizedBox(height: 20,),

                        Container(
                            alignment: Alignment.centerLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text('Color', style: TextStyle(fontSize: 20, color: Colors.black),),
                                ToggleButtons(
                                  direction: Axis.horizontal,
                                  onPressed: (int index) {
                                    setState(() {
                                      for (int i = 0; i < _selectedColor.length; i++) {
                                        _selectedColor[i] = i == index;
                                      }
                                    });
                                  },
                                  borderRadius: const BorderRadius.all(Radius.circular(8)),
                                  selectedBorderColor: Colors.green[700],
                                  selectedColor: Colors.white,
                                  fillColor: Colors.green[200],
                                  color: Colors.green[400],
                                  constraints: const BoxConstraints(
                                    minHeight: 40.0,
                                    minWidth: 80.0,
                                  ),
                                  isSelected: _selectedColor,
                                  children: color,
                                ),
                              ],
                            )
                        ),

                      ],
                  ),
                  )
                )

              ],
            ),
          ),
        ),
        floatingActionButton: SizedBox(
          width: 160,
          child: FloatingActionButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => const Cart1()));
            },
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Icon(Icons.add_shopping_cart),
                SizedBox(width: 5),
                Text('Add to cart'),
              ],
            ),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      ),

    );
  }
}

class SlidingImages extends StatefulWidget {
  const SlidingImages({Key? key});

  @override
  _SlidingImagesState createState() => _SlidingImagesState();
}

class _SlidingImagesState extends State<SlidingImages> {
  final PageController _pageController = PageController(initialPage: 0);
  final List<String> _images = [
    'assets/images/w1.jpg',
    'assets/images/w2.jpg',
    'assets/images/w3.png',
  ];

  @override
  Widget build(BuildContext context) {
    return PageView.builder(
      controller: _pageController,
      physics: const AlwaysScrollableScrollPhysics(), // Enable sliding
      itemCount: _images.length,
      onPageChanged: (int index) {
        setState(() {
          _pageController.animateToPage(index, duration: const Duration(milliseconds: 500), curve: Curves.ease); // Update the page controller
        });
      },
      itemBuilder: (BuildContext context, int index) {
        return ClipRRect(
          borderRadius: BorderRadius.circular(10), // Adjust the border radius as desired
          child: Image.asset(
            _images[index],
            width: 300,
            height: 300,
            fit: BoxFit.cover,
          ),
        );
      },
    );
  }
}
